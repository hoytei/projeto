<?php

use Illuminate\Database\Seeder;
use App\Entities\Tag;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrTags = [
            'inverno',
            'verão',
            'primavera',
            'outono',
            'calça',
            'saia',
            'jeans',
            'blusa',
            'casaco',
            'meia-calça',
            'tênis',
            'bota',
            'sapatilha',
            'vestido',
            'sandália',
            'colar',
            'cachecol',
            'chapéu',
            'bomber jacket'
            
        ];
        
        foreach($arrTags as $tagName){
            Tag::create(['name'=>$tagName]);
        }
    }
}
