<?php

use Illuminate\Database\Seeder;
use App\Entities\DocCategory;
use App\Entities\DocRoute;

class DocSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocCategory::truncate();
        
        $arrCategories = [
            1=>'User',
            2=>'Tag',
            3=>'Image'
        ];
        
        foreach($arrCategories as $category){
            DocCategory::create(['name'=>$category]);
        }
        
        DocRoute::truncate();
        DocRoute::firstOrCreate(
            [
                'id_doc_category'=>'1',
                'name'=>'CADASTRAR USUÁRIO',          
                'route'=>'/v1/users',        
                'method'=>'POST',        
                'description'=>'Cadastra um novo usuário no sistema.',        
                'parameters'=>'{
    "name":"Fulano",
    "email":"fulano@email.com",
    "password":"1234",
    "password_confirmation":"1234"
}',
                'return'=>'{
    "status": true,
    "status_code": 200,
    "message": "Usuário cadastrado com sucesso.",
    "data": {
        "name": "Fulano",
        "email": "fulano@email.com",
        "password": "81dc9bdb52d04dc20036dbd8313ed055",
        "id": 3
    }
}']);
        DocRoute::firstOrCreate(
            [
                'id_doc_category'=>'1',
                'name'=>'LOGIN',          
                'route'=>'/v1/login',        
                'method'=>'POST',        
                'description'=>'Efetua o login e recebe o token de autenticação.',        
                'parameters'=>'{
    "email":"fulano@email.com",
    "password":"1234"
}',
                'return'=>'{
    "status": true,
    "status_code": 200,
    "message": "Login efetuado com sucesso.",
    "data": {
        "user": {
            "id": 3,
            "name": "Fulano",
            "email": "fulano@email.com",
            "password": "81dc9bdb52d04dc20036dbd8313ed055"
        },
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzNTkzZGIyZGRiZDRlYyIsImRhdGEiOnsiaWQiOjMsIm5hbWUiOiJGdWxhbm8iLCJlbWFpbCI6ImZ1bGFub0BlbWFpbC5jb20iLCJwYXNzd29yZCI6IjgxZGM5YmRiNTJkMDRkYzIwMDM2ZGJkODMxM2VkMDU1In19.dobns9js6KJQC9zAZBtW0q9T6hC8tJgTT81iK761Gsk"
    }
}']);
        DocRoute::firstOrCreate(
            [
                'id_doc_category'=>'2',
                'name'=>'TAGS',          
                'route'=>'/v1/tags',        
                'method'=>'GET',        
                'description'=>'Obtem a listagem das tags cadastradas no sistema.',        
                'parameters'=>'***token no header',
                'return'=>'[
    {
        "id": 1,
        "name": "inverno"
    },
    {
        "id": 2,
        "name": "verão"
    },
    {
        "id": 3,
        "name": "primavera"
    }
]']);
        
        DocRoute::firstOrCreate(
            [
                'id_doc_category'=>'3',
                'name'=>'CADASTRO DE IMAGEM',          
                'route'=>'/v1/images',        
                'method'=>'POST',        
                'description'=>'Cadastra uma nova imagem no sistema.',        
                'parameters'=>'***token no header
{                    
    "image": (file),
    "tags":[7,8]
}',
                'return'=>'{
    "status": true,
    "status_code": 200,
    "message": "Usuário cadastrado com sucesso.",
    "data": {
        "name": "Koala.jpg",
        "file_name": "1-594afec228eec.jpg",
        "user_id": 1,
        "id": 23,
        "url": "http://localhost/projeto/back/look-do-dia/public/images/1/1-594afec228eec.jpg"
    }
}']);
        
        DocRoute::firstOrCreate(
            [
                'id_doc_category'=>'3',
                'name'=>'ÚLTIMAS IMAGENS',          
                'route'=>'/v1/images/news',        
                'method'=>'GET',        
                'description'=>'Retorna as últimas imagens cadastras no sistema.',        
                'parameters'=>'***token no header',
                'return'=>'{
    "status": true,
    "status_code": 200,
    "message": "Usuário cadastrado com sucesso.",
    "data": [
        {
            "id": 23,
            "name": "Koala.jpg",
            "file_name": "1-594afec228eec.jpg",
            "user_id": 1,
            "url": "http://localhost/projeto/back/look-do-dia/public/images/1/1-594afec228eec.jpg"
        },
        {
            "id": 22,
            "name": "Koala.jpg",
            "file_name": "1-594af10c0f974.jpg",
            "user_id": 1,
            "url": "http://localhost/projeto/back/look-do-dia/public/images/1/1-594af10c0f974.jpg"
        }
}']);
        
    }
}
