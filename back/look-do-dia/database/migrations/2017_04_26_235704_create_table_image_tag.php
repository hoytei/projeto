<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImageTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            
            $table->foreign('image_id')
                  ->references('id')
                  ->on('tag')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');
            
            $table->foreign('tag_id')
                  ->references('id')
                  ->on('image')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_tag');
    }
}
