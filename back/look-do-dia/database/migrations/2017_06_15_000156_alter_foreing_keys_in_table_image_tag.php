<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeingKeysInTableImageTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('image_tag', function (Blueprint $table) {
            $table->dropForeign('image_tag_image_id_foreign');
            $table->dropForeign('image_tag_tag_id_foreign');
            
            $table->foreign('image_id')
                  ->references('id')
                  ->on('image')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');
            
            $table->foreign('tag_id')
                  ->references('id')
                  ->on('tag')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
