<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocRoute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_route', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_doc_category');
            $table->string('name');
            $table->string('route');
            $table->string('method');
            $table->text('description');
            $table->text('parameters');
            $table->text('return');
            $table->index(['id_doc_category']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doc_route');
    }
}
