<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "image";
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $appends = ['url'];
    
    
    public function getUrlAttribute()
    {
        return url('images/'.$this->user_id.'/'.$this->file_name);
    }
    
    public function tags()
    {
        return $this->belongsToMany('App\Entities\Tag', 'image_tag', 'image_id', 'tag_id');
    }
}
