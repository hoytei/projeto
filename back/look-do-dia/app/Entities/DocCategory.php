<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DocCategory extends Model{

    protected $table = "doc_category";

    protected $guarded = array("id");

    public function doc_route()
    {
        return $this->hasMany('App\Entities\DocRoute','id_doc_category');
    }
}