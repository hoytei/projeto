<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DocRoute extends Model{

    protected $table = "doc_route";

    protected $guarded = array("id");

    public function doc_category()
    {
        return $this->belongsTo('App\Entities\DocCategory', 'id_doc_category');
    }
}