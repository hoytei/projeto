<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\Contracts\LoginServiceContract', 'App\Services\LoginService');
        $this->app->bind('App\Services\Contracts\UserServiceContract', 'App\Services\UserService');
        $this->app->bind('App\Services\Contracts\TagServiceContract', 'App\Services\TagService');
        $this->app->bind('App\Services\Contracts\ImageServiceContract', 'App\Services\ImageService');
        $this->app->bind('App\Services\Contracts\DocumentationServiceContract', 'App\Services\DocumentationService');
    }
}
