<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Contracts\UserRepositoryContract', 'App\Repositories\UserRepository');
        $this->app->bind('App\Repositories\Contracts\ImageRepositoryContract', 'App\Repositories\ImageRepository');
        $this->app->bind('App\Repositories\Contracts\TagRepositoryContract', 'App\Repositories\TagRepository');
        $this->app->bind('App\Repositories\Contracts\DocCategoryRepositoryContract', 'App\Repositories\DocCategoryRepository');
    }
}
