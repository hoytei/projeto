<?php

namespace App\Providers;

use App\Entities\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

use Firebase\JWT\JWT;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->header('token')) {
                try{
                    $token = $request->header('token');
                    $jwt = JWT::decode($token, env('JWT_KEY'), array('HS256'));
                    return User::find($jwt->data->id)->first();
                } catch (\Exception $e){
                    return null;
                }
            }
        });
    }
}
