<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils;

use Response;
use \Illuminate\Http\Response as ResponseBase;

/**
 * Description of RestResponse
 *
 * @author nicolas
 */
class RestResponse extends ResponseBase
{
    public static function respond($data, $headers = []) {
        return response()->json($data, $data['status_code'],$headers);
    }
    
    public static function success($message, $data = null) {
        return self::respond([
            'status' => true,
            'status_code' => parent::HTTP_OK,
            'message' => $message,
            'data' => $data
        ]);
    }
    
    public static function warningError($message, $status_code = parent::HTTP_BAD_REQUEST) {
        return self::respond([
            'status' => false,
            'status_code' => $status_code,
            'message' => $message,
        ]);
    }
    
    public static function notFound($message = 'Not Found!') {
        return self::respond([
            'status' => false,
            'status_code' => parent::HTTP_NOT_FOUND,
            'message' => $message,
        ]);
    }
    
    public static function internalError($message=null) {
        return self::respond([
            'status' => false,
            'status_code' => parent::HTTP_INTERNAL_SERVER_ERROR,
            'message' => ( $message ? $message : parent::$statusTexts[parent::HTTP_INTERNAL_SERVER_ERROR]),
        ]);
    }
    
    public static function validationError($message,$errors, $status_code = parent::HTTP_BAD_REQUEST) {
        return self::respond([
            'status' => false,
            'status_code' => $status_code,
            'message' => $message,
            'errors' => $errors
        ]);
    }
    
    public static function forbiddenError($message) {
        return self::respond([
            'status' => false,
            'status_code' => parent::HTTP_FORBIDDEN,
            'message' => $message
        ]);
    }
}
