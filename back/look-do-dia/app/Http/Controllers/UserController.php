<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Contracts\UserServiceContract;
use App\Utils\RestResponse;

class UserController extends Controller
{
    private $userService;
    
    public function __construct(UserServiceContract $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        return 'lista de usuários';
    }
    
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:user,email',
            'password' => 'required|confirmed'
        ]);
        $result = $this->userService->createNewUser($request->all());
        
        return RestResponse::success('Usuário cadastrado com sucesso.', $result);
    }
}
