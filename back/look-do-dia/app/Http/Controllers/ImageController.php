<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utils\RestResponse;


use App\Services\Contracts\ImageServiceContract;


class ImageController extends Controller
{
    private $imageService;
    
    public function __construct(ImageServiceContract $imageService)
    {
        $this->imageService = $imageService;
    }

    public function index()
    {
        $result = $this->imageService->getLastImages(12);
        return RestResponse::success('Usuário cadastrado com sucesso.', $result);
    }
    
    public function store(Request $request)
    {
        dd($request->all());
        $this->validate($request, [
            'file' => 'required|image|file'
        ]);
        $tagIds = ($request->has('tags'))? $request->tags : [];
        $result = $this->imageService->uploadImage($request->file('file'), $request->user(), $tagIds);
        return RestResponse::success('Imagem bacana.', $result);
    }
}
