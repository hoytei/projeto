<?php

namespace App\Http\Controllers;

use App\Services\Contracts\DocumentationServiceContract;

class DocumentationController extends Controller
{
    private $documentationService;

    public function __construct(DocumentationServiceContract $documentationService)
    {
        $this->documentationService = $documentationService;
    }

    public function show($categoryId)
    {
        $categories = $this->documentationService->getAllCategory();
        $currentCategory =  $this->documentationService->findCategoryById($categoryId);

        $dataToView = [
            'categories' => $categories,
            'currentCategory' => $currentCategory
        ];
        return view('doc.master', $dataToView);
    }
}
