<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Contracts\TagServiceContract;
use App\Utils\RestResponse;

class TagController extends Controller
{
    private $tagService;
    
    public function __construct(TagServiceContract $tagService)
    {
        $this->tagService = $tagService;
    }

    public function index()
    {
        return $this->tagService->getAllTags();
    }
}
