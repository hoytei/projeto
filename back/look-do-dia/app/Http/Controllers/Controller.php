<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function responseDefault($data,$message,$code=200,$erro=false)
    {
        $json = [
            'erro' => $erro,
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];
            
        return response()->json($json, $code);
    }
}
