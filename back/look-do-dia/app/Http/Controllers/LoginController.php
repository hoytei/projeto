<?php

namespace App\Http\Controllers;

use App\Services\Contracts\LoginServiceContract;
use Illuminate\Http\Request;
use App\Utils\RestResponse;

class LoginController extends Controller
{
    private $loginService;


    public function __construct(LoginServiceContract $loginService)
    {
        $this->loginService = $loginService;
    }

    public function login(Request $request)
    {
        if(!$request->has('email')){
            abort(400,'O campo email é obrigatório.');
        }
        if(!$request->has('password')){
            abort(400,'A senha é obrigatória.');
        }
        
        $result = $this->loginService->login($request->email, $request->password);
        return RestResponse::success('Login efetuado com sucesso.', $result);
    }
}
