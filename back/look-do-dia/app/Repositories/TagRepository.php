<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Repositories;

use App\Repositories\Contracts\TagRepositoryContract;
use App\Entities\Tag;


/**
 * Description of TagRepository
 *
 * @author alu201521476
 */
class TagRepository extends EntityRepository implements TagRepositoryContract
{
    public function __construct(Tag $tag)
    {
        parent::__construct($tag);
    }
}
