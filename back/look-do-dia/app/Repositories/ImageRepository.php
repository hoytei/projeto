<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Repositories\Contracts\ImageRepositoryContract;
use App\Entities\Image;

/**
 * Description of ImageRepository
 *
 * @author alu201521476
 */
class ImageRepository extends EntityRepository implements ImageRepositoryContract
{
    public function __construct(Image $image)
    {
        parent::__construct($image);
    }

    public function getLastImages($quantImages)
    {
        return $this->entity->orderBy('id', 'desc')->take($quantImages)->get();
    }
    
    public function linkTagToImage($unit, array $tagIds) 
    {
        $unit->tags()->sync($tagIds);
    }

}
