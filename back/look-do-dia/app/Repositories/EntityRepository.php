<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use app\Repositories\Contracts\EntityRepositoryContract;

abstract class EntityRepository implements EntityRepositoryContract
{
    protected $entity;
    public function __construct(Model $entity)
    {
        $this->entity = $entity;
    }
    public function getAll(array $fields = array())
    {
        if(count($fields)){
            return $this->entity->select($fields)->get();
        }
        return $this->entity->all();
    }
    public function find($id, array $fields = array())
    {
        if(count($fields)){
            return $this->entity->select($fields)->find($id);
        }
        return $this->entity->find($id);
    }
    public function create($arr)
    {
        return $this->entity->create($arr);
    }
    public function firstOrCreate($arrKV)
    {
        return $this->entity->firstOrCreate($arrKV);
    }
    public function updateByIdAndArrKeyValue($id,$arrKV)
    {
        $obj = $this->model->find($id);
        $obj->update($arrKV);
        return $obj;
    }
    public function deleteById($id)
    {
        $obj = $this->model->find($id);
        if(!$obj){
            return false;
        }
        return $obj->delete();
    }
    public function existsByArrKeyValue($arrKV)
    {
        return $this->model->where($arrKV)->exists();
    }
}