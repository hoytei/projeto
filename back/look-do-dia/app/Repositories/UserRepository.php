<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Repositories\Contracts\UserRepositoryContract;
use App\Entities\User;

/**
 * Description of UserRepository
 *
 * @author alu201521476
 */
class UserRepository extends EntityRepository implements UserRepositoryContract
{
    public function __construct(User $user)
    {
        parent::__construct($user);
    }
    
    public function findByEmail($email)
    {
        return User::where('email',$email)->first();
    }
}
