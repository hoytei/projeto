<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Contracts;

/**
 *
 * @author alu201521476
 */
interface EntityRepositoryContract 
{
    
    public function getAll(array $fields = array());
    
    public function find($id, array $fields = array());
    
    public function create($arr);
    
    public function firstOrCreate($arrKV);
    
    public function updateByIdAndArrKeyValue($id,$arrKV);
    
    public function deleteById($id);
    
    public function existsByArrKeyValue($arrKV);
    
}
