<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Contracts;

/**
 *
 * @author alu201521476
 */
interface ImageRepositoryContract extends EntityRepositoryContract
{
    public function linkTagToImage($image, array $tagIds);
}
