<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Repositories;

use App\Repositories\Contracts\DocCategoryRepositoryContract;
use App\Entities\DocCategory;


/**
 * Description of TagRepository
 *
 * @author alu201521476
 */
class DocCategoryRepository extends EntityRepository implements DocCategoryRepositoryContract
{
    public function __construct(DocCategory $tag)
    {
        parent::__construct($tag);
    }
}
