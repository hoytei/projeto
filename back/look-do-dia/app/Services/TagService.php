<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;
use App\Services\Contracts\TagServiceContract;

use App\Repositories\Contracts\TagRepositoryContract;
/**
 * Description of LoginService
 *
 * @author alu201521476
 */
class TagService implements TagServiceContract
{
    private $tagRepository;
    
    public function __construct(TagRepositoryContract $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function getAllTags() {
        return $this->tagRepository->getAll();
    }

}
