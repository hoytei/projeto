<?php

namespace App\Services;

use App\Services\Contracts\DocumentationServiceContract;
use App\Repositories\Contracts\DocCategoryRepositoryContract;

class DocumentationService implements DocumentationServiceContract
{
    private $docCategoryRepository;

    public function __construct(DocCategoryRepositoryContract $docCategoryRepository)
    {
        $this->docCategoryRepository = $docCategoryRepository;
    }

    public function getAllCategory()
    {
        return $this->docCategoryRepository->getAll();
    }

    public function findCategoryById($categoryId)
    {
        return $this->docCategoryRepository->find($categoryId);
    }
}