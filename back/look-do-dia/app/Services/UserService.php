<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;
use App\Services\Contracts\UserServiceContract;

use App\Repositories\Contracts\UserRepositoryContract;
/**
 * Description of LoginService
 *
 * @author alu201521476
 */
class UserService implements UserServiceContract
{
    private $userRepository;
    
    public function __construct(UserRepositoryContract $userRepositoryContract)
    {
        $this->userRepository = $userRepositoryContract;
    }
    
    public function createNewUser($post) {
        $data = [
          'name' => $post['name'],  
          'email' => $post['email'],  
          'password' => md5($post['password'])
        ];
        $user = $this->userRepository->create($data);
        if(!$user){
            abort(400,'Ocorreu um erro para cadastrar o usuário.');
        }
        return $user;
    }
    
}
