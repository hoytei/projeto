<?php

namespace App\Services\Contracts;

interface DocumentationServiceContract
{
    public function getAllCategory();

    public function findCategoryById($categoryId);
}