<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Contracts;

/**
 *
 * @author alu201521476
 */
interface ImageServiceContract {
    public function uploadImage($file, $user, array $tags);
}
