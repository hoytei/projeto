<?php

namespace App\Services\Contracts;

/**
 *
 * @author alu201521476
 */
interface TagServiceContract {
    public function getAllTags();
}
