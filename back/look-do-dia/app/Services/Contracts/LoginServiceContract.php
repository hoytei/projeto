<?php

namespace App\Services\Contracts;

/**
 *
 * @author alu201521476
 */
interface LoginServiceContract {
    public function login($email, $password);
}
