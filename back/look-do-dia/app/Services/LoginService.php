<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;
use App\Services\Contracts\LoginServiceContract;
use Firebase\JWT\JWT;

use App\Repositories\Contracts\UserRepositoryContract;
/**
 * Description of LoginService
 *
 * @author alu201521476
 */
class LoginService implements LoginServiceContract
{
    private $userRepository;
    
    public function __construct(UserRepositoryContract $userRepositoryContract)
    {
        $this->userRepository = $userRepositoryContract;
    }
    
    public function login($email, $password) 
    {
        $user = $this->userRepository->findByEmail($email);
        if(!$user){
            abort(401, 'Email não registrado.');
        }
        if(!$this->passwordIsCorrect($user, $password)){
            abort(401, 'Senha incorreta.');
        }
        $jwt = $this->getTokenJWT($user);
        return ['user'=>$user, 'token'=>$jwt];
    }
    
    private function passwordIsCorrect($user, $password)
    {
        if($user->password == md5($password)){
            return true;
        }
        return false;
    }
    
    private function getTokenJWT($user)
    {
        try{
            $key = env('JWT_KEY');
            
            $token['jti'] = uniqid($user->id);
            $token['data'] = $user;
            
            $jwt = JWT::encode($token, $key);
            return $jwt;
        } catch (\Exception $e){
            abort(400, $e->getMessage());
        }
    }
}
