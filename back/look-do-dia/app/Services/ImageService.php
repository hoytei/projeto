<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Services\Contracts\ImageServiceContract;
use App\Repositories\Contracts\ImageRepositoryContract;
/**
 * Description of ImageService
 *
 * @author alu201521476
 */
class ImageService implements ImageServiceContract
{
    private $imageRepository;
    
    public function __construct(
        ImageRepositoryContract $imageRepository
    ) 
    {
        $this->imageRepository = $imageRepository;
    }

    public function getLastImages($quantImages)
    {
        return $this->imageRepository->getLastImages($quantImages);
    }


    public function uploadImage($file, $user, array $tagIds=[]) {
        $destinationPath = base_path("public/images/$user->id");
        $fileName = $user->id.'-'.uniqid().'.'.$file->getClientOriginalExtension();
        $name = $file->getClientOriginalName();
        $file->move($destinationPath,$fileName);
        
        $image = $this->createImage($name, $fileName, $user->id);
        if(count($tagIds)){
            $this->imageRepository->linkTagToImage($image, $tagIds);
        }
        return $image;
    }
    
    private function createImage($name, $fileName, $userId)
    {
        $data = [
            'name'=>$name,
            'file_name'=>$fileName,
            'user_id'=>$userId
        ];
        return $this->imageRepository->create($data);
    }
}
