<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => '/v1'], function() use ($app)
{
    $app->post('/login', 'LoginController@login');
    
    ### USER ###
    $app->group(['prefix' => '/users'], function() use ($app){
        $app->get('/','UserController@index');
        $app->post('/','UserController@store');
    });
    
    
    ### TAG ###
    $app->group(['prefix' => '/tags', 'middleware'=>'auth'], function() use ($app){
        $app->get('/','TagController@index');
    });
    

    ### IMAGE ###
    $app->group(['prefix' => '/images', 'middleware'=>'auth'], function() use ($app){
        $app->get('/news','ImageController@index');
        $app->post('/','ImageController@store');
    });
    
    #################
    # DOCUMENTACAO
    #################
    $app->group(['prefix' => '/doc'], function () use ($app) {
        $app->get('/', function(){
            return redirect()->route('doc_category',['categoryId'=>'1']);
        });
        $app->get('/category/{categoryId}', ['uses'=>'DocumentationController@show', 'as'=>'doc_category']);
    });
});