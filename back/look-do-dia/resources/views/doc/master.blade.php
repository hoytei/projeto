<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link href="{{url('css/doc/style.css')}}" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                        DOCUMENTAÇÃO
                </li>
                @foreach($categories as $category)
                <li >
                    <a href="{{route('doc_category',['categoryId'=>$category->id])}}" class="{{($category->id == $currentCategory->id)? 'active' : ''}}">{{$category->name}}</a>
                </li>
                @endForeach
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                    @foreach($currentCategory->doc_route as $route)
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <p class="lead">{{$route->name}}</p>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                    <p><strong>Descrição:</strong> {{$route->description}} </p>
                                    <p><strong>Rota:</strong>  {{$route->route}} </p>
                                    <p><strong>Método:</strong>  {{$route->method}} </p>
                                    <p><strong>Parâmetros ex:</strong></p>
                                    <pre>{{$route->parameters}}</pre>
                                    <p><strong>Retorno ex:</strong></p>
                                    <pre>{{$route->return}}</pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    {{-- <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> --}}
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

   <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>