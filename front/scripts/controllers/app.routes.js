 (function () {
    'use strict';
    angular.module('app')
        .config(function($routeProvider) { 
            $routeProvider 
            .when("/dashboard", {
                templateUrl : "views/dashboard.html"
            })  
            .when("/user", {
                templateUrl : "views/user.html"
            })      
            .when("/signin", {
                templateUrl : "views/signin.html"
            })
             .when("/", {
                templateUrl : "views/login.html"
            })
            .when("/commitment_detail/:ID", {
                templateUrl : "views/commitment_detail.html"
            });
           
        });
})();