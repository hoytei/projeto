(function () {
    'use strict';
    angular.module('app')
        .controller('dashboardCtrl', ['$scope', '$http', '$rootScope', 'ImageService', dashboardCtrl])        

    function dashboardCtrl($scope, $http, $rootScope, ImageService) {        
        
        $scope.init = function(){
            $rootScope.showHeader = true;
            $scope.imageService = new ImageService($scope);
            $scope.getImages();
        }

        $scope.verificaSeNaoFoiPreenchido = function(variavel){ 
                if(variavel == null || variavel == '')
                    return true
                
                return false
        }

        $scope.getImages = function(){
            $scope.imageList = [];
           
            $scope.imageService.getRecentImages(function(result){                 
                // if(result.status != 200){
                //     $scope.userNotFound = true;
                //     return;
                // }
                $scope.imageList = result;
                
            });
       
         }

        $scope.init();
        
    };


})(); 