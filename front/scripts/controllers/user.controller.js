(function () {
    'use strict';
    angular.module('app')
        .controller('userCtrl', ['$scope', '$http', '$rootScope', 'ImageService', 'TagsService','FileUploader', userCtrl])        

    function userCtrl($scope, $http, $rootScope, ImageService, TagsService,FileUploader) {        
        
        $scope.init = function(){ 
            $rootScope.showHeader = true;
            $scope.imageService = new ImageService($scope);
            $scope.tagsService = new TagsService($scope);
            $scope.tagsList = [];
            $scope.selectedTagList = [];
            $scope.selectedTagId = [];
            //$scope.getImages();
            $scope.imageList = [];
            $scope.getTagList();
            var token = localStorage.getItem('token');
            $scope.uploader = new FileUploader({
                headers: {
                       'token': token
                     },     
                     formData: [{n:123}],
                url: 'http://localhost/projeto/back/look-do-dia/public/v1/images'
            });
            $scope.uploader.onBeforeUploadItem = function(){
                    $scope.uploader.formData.push({tags : 'asdasd'});
            };

    
            for(var i = 0; i< 10; i++){  
            if(i!=1)         
              $scope.imageList.push({number : i+1}) 
            }

            //alert(JSON.stringify($scope.imageList ))
        }

        $scope.verificaSeNaoFoiPreenchido = function(variavel){ 
                if(variavel == null || variavel == '')
                    return true
                
                return false
        }

        function getRandomInt(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min)) + min;
        }



        // $scope.getImages = function(){
        //     $scope.imageList = [];
           
        //     $scope.imageService.getRecentImages(function(result){                 
        //         // if(result.status != 200){
        //         //     $scope.userNotFound = true;
        //         //     return;
        //         // }
        //         $scope.imageList = result;
                
        //     });
       
        //  }

         $scope.selectedTag = function(id){ 
          
            for(var i = 0; i < $scope.tagsList.length; i++){
                if($scope.selected_tag ==  $scope.tagsList[i].name){
                    $scope.selectedTagList.push($scope.tagsList[i]);
                    $scope.selectedTagId.push($scope.tagsList[i].id);

                }
            }
           
         }

         $scope.removeTag = function(id){
            for(var i = 0; i < $scope.tagsList.length; i++){
                if(id ==  $scope.tagsList[i].id){
                    var index = $scope.selectedTagList.indexOf($scope.tagsList[i]);
                    $scope.selectedTagList.splice(index, 1);   
                    $scope.selectedTagId.push(index, 1);             

                }
            }
         }

         $scope.getTagList = function(){
            
            $scope.tagsService.getTags(function(result){
                $scope.tagsList = result.data;
            })
         }

        $scope.upladImage = function() {

            $("input[id='file']").click();
         

         
        }

        $scope.save = function(){               
             //$scope.uploader.formData.push({'tags' : $scope.selectedTagId});
             console.log($scope.uploader);
             $scope.uploader.uploadAll();
        }
      


        $scope.init();
        
    };


})(); 