(function () {
    'use strict';
    angular.module('app')
        .factory('ImageService', ['$http', function ($http) {
        
            function ImageService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            ImageService.prototype.getRecentImages = function (callback) { 
                var url = "http://demo4145276.mockable.io/imagens";         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
             ImageService.prototype.getuserImages = function (id, callback) { 
                var url = "http://demo4145276.mockable.io/imagens" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            ImageService.prototype.saVE = function (json, callback) {
                var url = "http://demo0741009.mockable.io/save";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 
            return ImageService;
        }])

}) (); 


